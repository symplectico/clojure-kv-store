(ns clojure-netty.protocol-test
  (:require
    [clojure-netty.protocol :refer :all]
    [clojure.test :refer :all])
  (:import (org.jboss.netty.buffer ChannelBuffers)
           (java.nio.charset StandardCharsets)))

(defn channel-buffer-from-string [s]
  (let [cb (ChannelBuffers/copiedBuffer
             (->> s
                  (map byte)
                  (byte-array)
                  bytes))]
    cb))

(defn buffer-to-string [buffer]
  (.toString buffer StandardCharsets/UTF_8))

(deftest simple-string
  (testing "can parse simple string"
    (let [cb (channel-buffer-from-string "+123\r\n")
          result (parse-request cb)]
      (is (= result "123")))))

(deftest integer
  (testing "can parse integer"
    (let [cb (channel-buffer-from-string ":123\r\n")
          result (parse-request cb)]
      (is (= result 123))))
  (testing "can write integer"
    (let [cb (write-response 123)
          s (buffer-to-string cb)]
      (is (= s ":123\r\n")))))

(deftest binary
  (testing "can parse binary string"
    (let [cb (channel-buffer-from-string "$6\r\nfoobar\r\n")
          result (parse-request cb)]
      (is (= result "foobar")))
    (let [cb (channel-buffer-from-string "$5\r\nvalue\r\n")
          result (parse-request cb)]
      (is (= result "value"))))
  (testing "can write binary string"
    (let [cb (write-response "foobar")
          s (buffer-to-string cb)]
      (is (= s "$6\r\nfoobar\r\n")))))

(deftest array
  (testing "can parse array"
    (let [cb (channel-buffer-from-string "*3\r\n+a simple string element\r\n:12345\r\n$7\r\ntesting\r\n")
          result (parse-request cb)]
      (is (= result ["a simple string element" 12345 "testing"])))))

(deftest dict
  (testing "can parse dict"
    (comment (let [cb (channel-buffer-from-string
                        "%3\r\n+key1\r\n+value1\r\n+key2\r\n*2\r\n+value2-0\r\n+value2-1\r\n:3\r\n$7\r\ntesting\r\n")
                   result (parse-request cb)]
               (is (= result {"key1" "value1"
                              "key2" ["value2-0" "value2-1"]
                              3      "testing"}))))
    (let [cb (channel-buffer-from-string
               "%2\r\n$4\r\ntest\r\n:42\r\n$3\r\nkey\r\n$5\r\nvalue\r\n")
          result (parse-request cb)]
      (is (= result {"test" 42
                     "key"  "value"})))

    ))