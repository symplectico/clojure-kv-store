FROM clojure AS packager
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY project.clj /usr/src/app
RUN lein deps
COPY . /usr/src/app
RUN lein uberjar

FROM openjdk:8-jre-alpine
COPY --from=packager /usr/src/app/target/uberjar/clojure-netty-0.1.0-SNAPSHOT-standalone.jar app-standalone.jar
ENTRYPOINT ["java", "-jar", "app-standalone.jar"]