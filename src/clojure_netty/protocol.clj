(ns clojure-netty.protocol
  (:require [clojure.string :as s])
  (:import (org.jboss.netty.buffer ChannelBuffers)
           (java.nio.charset StandardCharsets)))

(declare parse-request)

(defn readline [channel-buffer]
  (loop [byte (.readByte channel-buffer)
         result []]
    (if-not (= (char byte) \return)
      (recur (.readByte channel-buffer)
             (conj result byte))
      (do
        (.skipBytes channel-buffer 1)
        result))))

(defn parse-simple-string [channel-buffer]
  (let [s (->> channel-buffer
               (readline)
               (map char)
               (apply str))]
    s))

(defn parse-error [channel-buffer])

(defn parse-integer [channel-buffer]
  (let [n (-> channel-buffer
              (parse-simple-string)
              (Long/parseLong))]
    n))

(defn parse-string [channel-buffer]
  (let [length (parse-integer channel-buffer)]
    (if (= length -1)
      nil
      (loop [byte (.readByte channel-buffer)
             result []
             cnt length]
        (if (> cnt 0)
          (recur (.readByte channel-buffer)
                 (conj result byte)
                 (dec cnt))
          (do
            (.skipBytes channel-buffer 1)
            (->> result
                 (map char)
                 (apply str))))))))

(defn parse-array [channel-buffer]
  (let [num (parse-integer channel-buffer)]
    (into []
          (for [_ (range num)]
            (parse-request channel-buffer)))))

(defn parse-dict [channel-buffer]
  (let [num (parse-integer channel-buffer)
        elts (for [_ (range (* 2 num))]
               (parse-request channel-buffer))]
    (apply array-map elts)))

(defn parse-request [channel-buffer]
  (let [first-char (char (.readByte channel-buffer))
        result (case first-char
                 \+ (parse-simple-string channel-buffer)
                 \- (parse-error channel-buffer)
                 \: (parse-integer channel-buffer)
                 \$ (parse-string channel-buffer)
                 \* (parse-array channel-buffer)
                 \% (parse-dict channel-buffer))]
    result))

(declare write-response-intl)

(defn write-str-as-bytes [buffer string]
  (.writeBytes buffer
               (->> string
                    (map byte)
                    byte-array
                    bytes))
  buffer)

(defn write-simple-string [channel-buffer resp]
  (write-str-as-bytes channel-buffer (str "+" resp "\r\n")))

(defn write-array [channel-buffer resp]
  (write-str-as-bytes channel-buffer (str "*" (count resp) "\r\n"))
  (doseq [elt resp]
    (write-response-intl channel-buffer elt))
  channel-buffer)

(defn write-integer [channel-buffer resp]
  (write-str-as-bytes channel-buffer (str ":" resp "\r\n")))

(defn write-dict [channel-buffer resp]
  (write-str-as-bytes channel-buffer (str "%" (count resp) "\r\n"))
  (doseq [[k v] resp]
    (write-response-intl channel-buffer k)
    (write-response-intl channel-buffer v))
  channel-buffer)

(defn write-string [channel-buffer resp]
  (write-str-as-bytes channel-buffer (str "$" (count resp) "\r\n" resp "\r\n")))

(defn write-response-intl [channel-buffer resp]
  (cond (string? resp) (write-string channel-buffer resp)
        (vector? resp) (write-array channel-buffer resp)
        (number? resp) (write-integer channel-buffer resp)
        (map? resp) (write-dict channel-buffer resp)
        (nil? resp) (write-str-as-bytes channel-buffer "$-1\r\n")))

(defn write-response [resp]
  (let [channel-buffer (ChannelBuffers/dynamicBuffer)]
    (write-response-intl channel-buffer resp)))

