(ns clojure-netty.commands.string)

(defn append
  "Append a value to a key."
  [kv-store key value]
  (-> (swap! kv-store update key #(if (string? %)
                                    (str % value)
                                    %))
      (get key)
      (#(when (string? %)
          (count %)))))

(defn incrby
  "Increment the integer value of a key by the given amount."
  ([kv-store key increment parser]
   (let [[old new] (swap-vals! kv-store update key
                               #(try
                                  (let [value-as-num (parser %)
                                        n (parser increment)]
                                    (str (+ n value-as-num)))
                                  (catch NumberFormatException e
                                    %)))
         old-val (get old key)
         new-val (get new key)]
     (if (identical? old-val new-val)
       "error"
       new-val)))
  ([kv-store key increment]
   (incrby kv-store key increment #(Long/parseLong %)))
  ([kv-store key]
   (incrby kv-store key "1")))

(defn decr
  "Decrement the integer value of a key by one."
  [kv-store key]
  (incrby kv-store key "-1"))

(defn decrby
  "Decrement the integer value of a key by one."
  [kv-store key decrement]
  (incrby kv-store key (str "-" decrement)))

(defn strget
  "Get the value of a key."
  [kv-store key]
  (get @kv-store key))

(defn strgetdel
  "Get the value of a key and delete the key."
  [kv-store key]
  (-> (swap-vals! kv-store dissoc key)
      first
      (get key)))

(defn kv-getex
  "Get the value of a key and optionally set its expiration."
  [key])

(defn strgetrange
  "Get a substring of the string stored at a key."
  ([kv-store key start end]
   (-> @kv-store
       (get key)
       (subs (Long/parseLong start) (Long/parseLong end))))
  ([kv-store key start]
   (-> @kv-store
       (get key)
       (subs (Long/parseLong start)))))

(defn strgetset
  "Set the string value of a key and return its old value."
  [kv-store key value]
  (-> (swap-vals! kv-store assoc key value)
      first
      (get key)))

(defn incr
  "Increment the integer value of a key by one."
  [kv-store key]
  (incrby kv-store key))

(defn incrbyfloat
  "Increment the float value of a key by the given amount."
  [kv-store key increment]
  (incrby key increment #(Double/parseDouble %)))

(defn lcs
  "Find longest common substring."
  [key1 key2])

(defn strmget
  "Get the values of all the given keys."
  [kv-store & keys]
  (mapv #(get @kv-store %) keys))

(defn strmset
  "Set multiple keys to multiple values."
  [kv-store & keyvals]
  (swap! kv-store merge (apply hash-map keyvals))
  1)

(defn strmsetnx
  "Set multiple keys to multiple values, only if none of the keys exists."
  [kv-store & keyvals]
  (-> (swap-vals! kv-store
                  (fn [kv]
                    (let [keys-to-set (map first (partition 2 keyvals))]
                      (if (empty? (select-keys kv keys-to-set))
                        (merge kv (apply hash-map keyvals))
                        kv))))
      (#(if (identical? (first %) (second %))
          0
          1))))

(defn kv-psetnx
  "Set the vlaue and expiration in milliseconds of a key."
  [key milliseconds value])

(defn strset
  "Set the string value of a key."
  [kv-store key value]
  (swap! kv-store assoc key value)
  1)

(defn kv-setex
  "Set the value and expiration of a key."
  [key seconds value])

(defn strsetnx
  "Set the value of a key, only if the key does not exist."
  [kv-store key value]
  (-> (swap-vals! kv-store update key #(if (nil? %) value %))
      (#(if (get (first %) key)
          0
          1))))

(defn strsetrange
  "Overwrite part of string at key starting at the specified offset."
  [kv-store key offset value]
  (-> (swap! kv-store update key #(str (subs % 0 (Long/parseLong offset)) value))
      (get key)
      count))

(defn strstrlen
  "Get the length of the value stored in a key."
  [kv-store key]
  (-> (get @kv-store key)
      count))
