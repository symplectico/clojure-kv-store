(ns clojure-netty.commands.list)

(defn lindex
  "Get an element from a list by its index."
  [kv-store key idx]
  (-> @kv-store
      (get key)
      (nth idx)))

(defn linsert
  "Insert an element before or after another element in a list."
  [kv-store key relation pivot element]
  (-> (swap-vals! kv-store update key
                  (fn [list]
                    (let [idx (.indexOf list pivot)]
                      (if (not= idx -1)
                        (case relation
                          "BEFORE" (concat [element] (subvec list idx))
                          "AFTER" (concat (subvec list 0 (+ 1 idx)) [element]))
                        list))))
      ((fn [[old new]]
         (if (= -1 (.indexOf (get old key) pivot))
           -1
           (count (get new key)))))))

(defn llen
  "Get the length of a list."
  [kv-store key]
  (-> @kv-store
      (get key)
      count))

(defn lpoprpush [kv-store src dest]
  (-> (swap! kv-store
             (fn [kv]
               (let [elt (first (get kv src))]
                 (-> kv
                     (update src (comp vec rest))
                     (update dest #(vec (concat % [elt])))))))
      (get dest)
      last))

(defn rpoplpush [kv-store src dest]
  (-> (swap! kv-store
             (fn [kv]
               (let [elt (last (get kv src))]
                 (-> kv
                     (update src (comp vec butlast))
                     (update dest #(vec (concat [elt] %)))))))
      (get dest)
      first))

(defn lmove
  "Pop an element from a list, push it to another list and return it."
  [kv-store source destination src-loc dest-loc]
  (cond
    (and (= src-loc "LEFT")
         (= dest-loc "RIGHT"))
    (lpoprpush kv-store source destination)

    (and (= src-loc "RIGHT")
         (= dest-loc "LEFT"))
    (rpoplpush kv-store source destination)

    :else "error"))

(defn lmpop
  "Pop elements from a list."
  [kv-store & keys]
  (-> (swap-vals! kv-store #(apply dissoc % keys))
      first
      ((fn [old-value]
         (into []
               (comp (map #(get old-value %)) (remove nil?))
               keys)))))

(defn lpop
  "Remove and get the first elements in a list."
  ([kv-store key count]
   (let [count-n (Long/parseLong count)]
     (-> (swap-vals! kv-store update key #(subvec % count-n))
         first
         (get key)
         (subvec 0 count-n))))
  ([kv-store key]
   (lpop kv-store key "1")))

(defn lpos
  "Return the index of matching elements on a list."
  [kv-store key element & args]
  (let [opt-args (apply hash-map args)
        rank (if-let [_rank (get "RANK" opt-args)]
               (Long/parseLong _rank)
               1)
        cnt (if-let [_cnt (get "COUNT" opt-args)]
              (Long/parseLong _cnt)
              1)
        maxlen (if-let [_maxlen (get "MAXLEN" opt-args)]
                 (Long/parseLong _maxlen)
                 0)]
    )

  )

(defn lpush [kv-store key & vals]
  (swap! kv-store update key #(vec (concat vals %))))

(defn rpush [kv-store key & vals]
  (swap! kv-store update key #(vec (concat % vals))))


(defn rpop [kv-store key]
  (-> (swap-vals! kv-store update key butlast)
      first
      (get key)
      last))

(defn lrem [kv-store key value]
  (swap! kv-store update key (partial remove #(= % value))))


(defn lset [kv-store key idx value]
  (swap! kv-store update key assoc idx value))

(defn ltrim [kv-store key start stop]
  (-> (swap! kv-store update key subvec start stop)
      (get key)
      count))

(defn lrange
  ([kv-store key start end]
   (-> @kv-store
       (get key)
       (subvec start end)))
  ([kv-store key start]
   (-> @kv-store
       (get key)
       (subvec start))))

(defn lflush [kv-store key]
  (-> (swap-vals! kv-store assoc key [])
      first
      (get key)
      count))

(defn kv-pop [kv-store key]
  (-> (swap-vals! kv-store dissoc key)
      first
      (get key)))