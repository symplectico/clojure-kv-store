(ns clojure-netty.commands.generic)

(defn kv-delete
  "Delete a key."
  [kv-store key]
  (-> (swap-vals! kv-store dissoc key)
      first
      (get key)
      (#(if (nil? %) 0 1))))

(defn kv-mdelete
  "Delete a key."
  [kv-store & keys]
  (-> (swap-vals! kv-store #(apply dissoc % keys))
      first
      ((fn [old-value]
         (->> (map #(get old-value %) keys)
              (remove nil?)
              count)))))

(defn kv-exists
  "Determine if a key exists."
  [kv-store key]
  (-> @kv-store
      (get key)
      (#(if (nil? %) 0 1))))

(defn dbsize
  "Return the number of keys in the database."
  [kv-store]
  (count @kv-store))

(defn flushdb
  "Remove all keys from the database."
  [kv-store]
  (-> (reset-vals! kv-store {})
      first
      count))

