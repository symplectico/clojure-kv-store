(ns clojure-netty.server
  (:require [clojure-netty.protocol :refer [parse-request write-response]]
            [clojure-netty.handlers :refer [handle-request]])
  (:import
    [java.net InetSocketAddress]
    [java.util.concurrent Executors]
    [org.jboss.netty.bootstrap ServerBootstrap]
    [org.jboss.netty.channel SimpleChannelHandler]
    [org.jboss.netty.channel.socket.nio NioServerSocketChannelFactory]
    (org.jboss.netty.channel.group DefaultChannelGroup)))

(def all-channels (DefaultChannelGroup. "server"))

(defn make-handler
  []
  (proxy [SimpleChannelHandler] []
    (channelOpen [ctx e]
      (.add all-channels (.getChannel e)))
    (channelConnected [ctx e]
      (let [c (.getChannel e)]
        (println "Connected:" c)))
    (channelDisconnected [ctx e]
      (let [c (.getChannel e)]
        (println "Disconnected:" c)))
    (messageReceived [ctx e]
      (let [c (.getChannel e)
            cb (.getMessage e)
            request (parse-request cb)
            response (handle-request request)
            response-buffer (write-response response)]
        (println response)
        (.write c response-buffer)))
    (exceptionCaught [ctx e]
      (let [throwable (.getCause e)]
        (println "@exceptionCaught" throwable))
      (-> e .getChannel .close))))

(defn start-server
  [port handler]
  (let [channel-factory (NioServerSocketChannelFactory.
                          (Executors/newCachedThreadPool)
                          (Executors/newCachedThreadPool))
        bootstrap (ServerBootstrap. channel-factory)
        pipeline (.getPipeline bootstrap)]
    (.addLast pipeline "handler" handler)
    (.setOption bootstrap "child.tcpNoDelay" true)
    (.setOption bootstrap "child.keepAlive" true)
    (.add all-channels (.bind bootstrap (InetSocketAddress. port)))
    channel-factory))

(defn stop-server
  [channel-factory]
  (let [future (.close all-channels)]
    (.awaitUninterruptibly future)
    (.releaseExternalResources channel-factory)))