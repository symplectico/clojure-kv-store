(ns clojure-netty.handlers
  (:require [clojure-netty.store :refer [store]]
            [clojure-netty.commands.generic :refer :all]
            [clojure-netty.commands.list :refer :all]
            [clojure-netty.commands.string :refer :all]
            [clojure.string :as s])
  (:import (clojure.lang PersistentVector)
           (java.lang String)))


(def commands
  {:del         kv-mdelete
   :exists      kv-exists

   :dbsize      dbsize
   :flush       flushdb

   :append      append
   :decr        decr
   :decrby      decrby
   :get         strget
   :getdel      strgetdel
   :getrange    strgetrange
   :getset      strgetset
   :incr        incr
   :incrby      incrby
   :incrbyfloat incrbyfloat
   :mget        strmget
   :mset        strmset
   :msetnx      strmsetnx
   :set         strset
   :setnx       strsetnx
   :setrange    strsetrange
   :strlen      strstrlen

   :lindex      lindex
   :linsert     linsert
   :llen        llen
   :lmove       lmove

   })

(defn execute-command [cmd args]
  (comment (try
             (apply (cmd commands) store args)
             (catch Exception e
               (println (.getCause e))
               "error")))
  (apply (cmd commands) store args))

(defmulti handle-request #(type %))

(defmethod handle-request String [req]
  (-> req
      (s/split #"\s+")
      handle-request))

(defmethod handle-request PersistentVector [req]
  (execute-command (-> (first req)
                       s/lower-case
                       keyword)
                   (rest req)))