(ns clojure-netty.core
  (:gen-class)
  (:require [clojure-netty.client :as c]
            [clojure-netty.server :as s]
            [clojure.tools.cli :refer [parse-opts]]))

(def cli-options
  [["-s" "--server" "Start in server mode"]
   ["-p" "--port PORT" "Port number"
    :default 5000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-h" "--help"]])

(defn -main
  [& args]
  (let [{:keys [options errors]} (parse-opts args cli-options)]
    (if (:server options)
      (do
        (println (str "Server started on port " (:port options)))
        (s/start-server (:port options) (s/make-handler)))
      (do
        (println "Client started...")
        (c/start-client (:port options) (c/make-handler))))))
