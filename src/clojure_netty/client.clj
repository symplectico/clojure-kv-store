(ns clojure-netty.client
  (:require [clojure-netty.protocol :refer [parse-request write-response]])
  (:import (org.jboss.netty.channel.socket.nio NioClientSocketChannelFactory)
           (java.util.concurrent Executors)
           (org.jboss.netty.bootstrap ClientBootstrap)
           (java.net InetSocketAddress)
           (org.jboss.netty.channel SimpleChannelHandler)))

(defn make-handler
  []
  (proxy [SimpleChannelHandler] []
    (messageReceived [ctx e]
      (let [c (.getChannel e)
            cb (.getMessage e)]
        (println (parse-request cb))
        (print "Prompt: ")
        (flush)))
    (exceptionCaught [ctx e]
      (let [throwable (.getCause e)]
        (println "@exceptionCaught" throwable))
      (-> e .getChannel .close))))

(defn start-client
  [port handler]
  (let [channel-factory (NioClientSocketChannelFactory.
                          (Executors/newCachedThreadPool)
                          (Executors/newCachedThreadPool))
        bootstrap (ClientBootstrap. channel-factory)
        pipeline (.getPipeline bootstrap)]
    (.addLast pipeline "handler" handler)
    (.setOption bootstrap "child.tcpNoDelay" true)
    (.setOption bootstrap "child.keepAlive" true)
    (let [channel (-> bootstrap
                      (.connect (InetSocketAddress. port))
                      (.sync)
                      (.getChannel))]
      (print "Prompt: ")
      (flush)
      (loop []
        (let [line (clojure.string/trim (read-line))]
          (if (= line "quit")
            :quit
            (do
              (.write channel (write-response line))
              (recur))))))))